<?php

return [
    "soap" => [
        "wsdl" => "http://currencyconverter.kowabunga.net/converter.asmx?WSDL",
        "currency_action" => "GetCurrencies",
        "conversion_action" => "GetConversionRate",
    ],

    "json" => [
        "url" => "http://api.currencylayer.com/",
        "key" => env('CURRENCY_LAYER_KEY'),
        "currency_action" => "list",
        "conversion_action" => "historical",
    ],

    /*  Currencies that will be displayed in the dropdown
    |   When empty all server currencies will be used
    */
    "defaultCurrencies" => [
        "USD","EUR","GBP","AUD","NOK","CHF"
    ],
];