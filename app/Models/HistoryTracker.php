<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryTracker extends Model
{
    use HasFactory;

    protected $fillable = ["currency_from", "currency_to", "user_id", "rate", "amount", "date"];

    protected $appends = ['feed'];

    protected $hidden = ['user_id'];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
    ];

    public function getFeedAttribute()
    {
        $date = date('d-m-Y', strtotime($this->date));
        $result = ($this->amount * $this->rate);
        return "$date: <span class='inline-block text-right w-1/12 pr-2'>$this->amount</span> ".
            "$this->currency_from -> $this->currency_to: <span class='w-1/12 inline-block text-right pr-3'>".
            "$result</span> [$this->rate]";
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}