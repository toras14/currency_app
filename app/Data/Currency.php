<?php

namespace App\Data;

class Currency
{
    private $from;

    private $to;

    private $amount; 

    private $date;

    public function __construct($data = [])
    {
        $this->from     = $data['from'] ?? null;
        $this->to       = $data['to'] ?? null;
        $this->amount   = $data['amount'] ?? null;
        $this->date     = $data['date'] ?? null;
    }

    public function setFrom($value)
    {
        return $this->from = $value;
    }

    public function setTo($value)
    {
        return $this->to = $value;
    }

    public function setAmount($value)
    {
        return $this->amount = $value;
    }

    public function setDate($value)
    {
        return $this->date = $value;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getDate()
    {
        return $this->date;
    }
}