<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;

class SoapCurrencyConverter implements ConverterContract
{
    private $wrapper;

    /**
     * @var SimpleSoapWrapper
     */
    private $client;

    public function __construct(SimpleSoapWrapper $wrapper)
    {
        $this->wrapper = $wrapper;
    }
    
    /**
     * @param string $function
     * 
     * @return array
     */
    public function getCurrencies(string $function): array
    {
        $this->initClient();
        
        if($this->client->functionExists($function)){
            $response = $this->client->call($function)->getResponse();
            
            if(property_exists($response, $function . 'Result')){
                $currencies = (array) $response->{$function . 'Result'};
                // dd(current($currencies));
                return $currencies['string'];
            }
        };
        return [];
    }

    private function initClient()
    {
        $this->client = $this->wrapper->setClient(config("converter.soap.wsdl"), [
            'trace' => 1,
            'user_agent' => 'PHPSoapClient',
            'connection_timeout'=> 15,
        ]);
    }

    /**
     * @param String $from
     * @param $date
     * 
     * 
     */
    public function getCurrencyRate(string $from, $date)
    {
        
    }

    protected function conversionData($from, $to, $amount, $date)
    {
        // return data dependent on soap wsdl
        return collect([
            "convert" => [
                "CurrencyFrom" => $from,
                "CurrencyTo" => $to,
                "RateDate" => $date,
                // "Amount" => $amount
            ]
        ]);
    }

    /**
     * @param string $from
     * @param string $to
     * @param $amount
     * @param $date
     * 
     */
    public function getConversion(string $function, string $from, string $to, $amount, $date): array
    {
        $data = $this->conversionData($from, $to, $amount, $date);
        
        $this->initClient();
        if($this->client->functionExists($function)){
            $response = $this->client->call($function, [
                $data->get('convert')
            ])->getResponse();

            if(property_exists($response, $function . 'Result')){
                $rate = $response->{$function . 'Result'};
                return [
                    "conversionRate" => $rate,
                    "conversionAmount" => $amount * $rate,
                ];
            }
        };
        return [];
    }
}