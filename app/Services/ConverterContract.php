<?php

namespace App\Services;

interface ConverterContract
{
    public function getCurrencies(string $function);

    public function getConversion(string $function, string $from, string $to, $amount, $date);
}