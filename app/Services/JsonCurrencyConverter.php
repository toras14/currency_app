<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;

class JsonCurrencyConverter implements ConverterContract
{
    private $access;

    private $url;

    private $endpoint;

    public function __construct()
    {
        $this->url = config('converter.json.url');
        $this->host = parse_url($this->url, PHP_URL_HOST);
        $this->access = '?access_key=' . config('converter.json.key');
    }

    public function getCurrencies(string $function): array
    {
        $currencies = $this->execCurl($function);
        if($currencies['success'] && array_key_exists('currencies', $currencies)){
            return array_keys($currencies['currencies']);
        }
        return [];
    }

    public function getConversion(string $function, string $from, string $to, $amount, $date)
    {
        $response = $this->conversionFromSource($function, $from, $to, $date);
        $rate = $response['quotes'][$from.$to];
        $amount = $rate * $amount;
        // $query = "&from=$from&to=$to&amount=$amount&date=$date"; // paid plan
        // $rates = $this->execCurl('convert', $query);

        return [
            "conversionRate" => $rate,
            "conversionAmount" => $amount,
        ];
    }

    public function conversionFromSource(string $function, string $from, string $to, $date)
    {
        // free plan can only use USD as source
        $query = "&source=$from&currencies=$to&date=$date";
        return $this->execCurl($function, $query);
    }

    /**
     * 
     */
    private function execCurl(string $endpoint, $query = null)
    {
        $ch = curl_init(
            $this->url . $endpoint . $this->access . $query
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        if($response === false){
            $response = [
                "success" => false,
                "error" => curl_error($ch),
            ];
        }
        curl_close($ch);

        return json_decode($response, true);
    }
}