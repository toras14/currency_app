<?php

namespace App\Services;

class ConvertionDetailsService
{
    public function get($function)
    {
        $function = request()->has('json') 
            ? config('converter.json.'.$function.'_action') 
            : config('converter.soap.'.$function.'_action');
        $key = request()->has('json') ? config('converter.json.url') : config('converter.soap.wsdl');

        return [
            "key" => parse_url($key, PHP_URL_HOST) . "_" . $function,
            "service" => request()->has('json') ? "json" : "soap",
            "function" => $function
        ];
    }
}