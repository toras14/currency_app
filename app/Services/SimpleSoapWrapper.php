<?php

namespace App\Services;

use SoapClient;
use App\Exceptions\NoClientException;
use App\Exceptions\SetClientException;
use Illuminate\Support\Facades\Cache;

class SimpleSoapWrapper
{
    private $client;

    private $wdsl;

    private $host;

    private $response;

    public function __construct()
    {
    }

    /**
     * @param $wdsl
     * @param array $options
     */
    public function setClient($wdsl, array $options = []): SimpleSoapWrapper
    {
        $this->wdsl = $wdsl;
        $this->host = parse_url($this->wdsl, PHP_URL_HOST);
        
        try {
            $this->client = new \SoapClient(
                $wdsl, 
                $options
            );
    
            return $this;
            
        } catch (SetClientException $e) {
            // echo $e->getMessage();

            return [
                "error" => $e->getMessage()
            ];
        }
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getFunctions()
    {
        if ($this->client) {
            return $this->client->__getFunctions();
        }
        throw new NoClientException("No client has been set.");
    }

    public function getFunctionNames()
    {
        $key = "$this->host-fNames";   
        // Cache::forget($key);
        if (Cache::store('file')->has($key)) {
            $fNames = (array) json_decode(Cache::store('file')->get($key));
            
        }else{
            $fNames = collect($this->getFunctions())->map(function ($method){
                return str_replace('Response', '', explode(' ', $method)[0]) ?? null;
            })->toArray();
            
            Cache::store('file')->put($key, json_encode($fNames), 60*60*24*30);
        }
        
        return $fNames;
    }

    public function functionExists($function)
    {
        return in_array($function, $this->getFunctionNames());
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setCache(int $time): SimpleSoapWrapper
    {
        $this->cache = $time;
        
        return $this;
    }

    public function call($function, $data = [], $options = null): SimpleSoapWrapper
    {
        if($this->client){
            
            $this->response = $this->client->__soapCall($function, $data);
                
            return $this;
        }

        throw new NoClientException("No client has been set.");
    }
}