<?php

namespace App\Http\Controllers;

use App\Models\HistoryTracker;
use Illuminate\Http\Request;
use App\Services\ConverterContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Services\ConvertionDetailsService;

class ConversionController extends Controller
{
    public function index()
    {
        
        return view('pages.conversion');
        // return view('test');
    }

    public function convert(Request $request, ConverterContract $converter, ConvertionDetailsService $details)
    {
        $valid = $request->validate([
            "from" => 'required|max:3',
            "to" => 'required|max:3|different:from',
            "amount" => ['required','integer','min:1','max:100000'], // float: ,'regex:/^\d+(\.\d{1,2})?$/'
            "date" => 'required|date|before_or_equal:today'
        ]);

        $data = $details->get('conversion');

        $key = $data['key'] . "_" . $valid['from'] . $valid['to'] . $valid['date'] . "_" . $valid['amount'];
        $cached = false;

        // Cache::forget($key);
        
        if (Cache::store('file')->has($key)) {
            $response = (array) json_decode(Cache::store('file')->get($key));
            $cached = true;
        }else{
            $response = $converter->getConversion(
                $data['function'], 
                $valid['from'], 
                $valid['to'], 
                $valid['amount'], 
                $valid['date']
            );
            
            if(count($response) > 0)Cache::store('file')->put($key, json_encode($response), 60*60*24);
        }

        $tracker = null;
        if (Auth::check() && $request->get('first_load') == false) {
            if(HistoryTracker::create([
                "currency_from" => $valid['from'],
                "currency_to" => $valid['to'],
                "user_id" => Auth::user()->id,
                "rate" => number_format($response['conversionRate'], 2),
                "amount" => $valid['amount'],
                "date" => $valid['date'],
            ])){
                $tracker = true;
            };
        }

        return response()->json(
            [
                'response' => array_merge(
                    $request->only('from','to','amount','date'), 
                    [   
                        "result" => $response,
                        "service" => $data['service'],
                        "cached" => $cached,
                        "key" => $key,
                        "logged_in" => Auth::check(),
                        "tracker" => $tracker,
                    ],
                ),
            ]
        );
    }
}