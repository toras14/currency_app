<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ConverterContract;
use Illuminate\Support\Facades\Cache;
use App\Services\ConvertionDetailsService;

class CurrencyController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ConvertionDetailsService $details, ConverterContract $wrapper, $param = null)
    {
        $data = $details->get('currency');
        $key = $data['key'];

        // Cache::forget($key);
        $cached = false;

        if (Cache::store('file')->has($key)) {
            $currencies = (array) json_decode(Cache::store('file')->get($key));
            $cached = true;
        }else{
            $currencies = $wrapper->getCurrencies($data['function']);
            if(count($currencies) > 0 && !array_key_exists('error', $currencies)){
                Cache::store('file')->put($key, json_encode($currencies), 60*60*24*100);
            }
        }

        $defaultCurrencies = config('converter.defaultCurrencies') ?? [];
        
        if(!array_key_exists('error', $currencies)){
            $currencies = !empty($defaultCurrencies) ? array_values(array_intersect(
                (array) $currencies, 
                config('converter.defaultCurrencies')
            )) : (array) $currencies;
        }
        
        return response()->json([
            "currencies" => $currencies ?? null,
            "cached" => $cached,
            "service" => $data['service'],
        ]);
    }
}