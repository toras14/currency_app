<?php

namespace App\Providers;

use App\Services\ConverterContract;
use App\Services\Greeting;
use App\Services\JsonCurrencyConverter;
use App\Services\SimpleSoapWrapper;
use App\Services\SoapCurrencyConverter;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ConverterContract::class, function(){
            // config('conversion');
            if(request()->has('json')){
                return new JsonCurrencyConverter();
            };
            return new SoapCurrencyConverter(new SimpleSoapWrapper);
        });

        $this->app->singleton(Greeting::class, function(){
            // config('greeter');
            return new Greeting('My greeting message: ');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}