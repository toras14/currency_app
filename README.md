## About project

Currency conversion app using **SOAP** and **JSON** requests.
Soap link is [Kowabunga](http://currencyconverter.kowabunga.net/converter.asmx). This one still works for free.
Json api is [Currencylayer](https://currencylayer.com/). For this one a api key is required. In this app it uses the free version. Api key should be included in the `.env` file as `CURRENCY_LAYER_KEY`. (also see `.env.example`)

It converts one currency to another. It will retrieve a list of available currencies from the api's and cache these in a file. 
To get a selection of the currencies we use the `converter.defaultCurrencies` key in the `converter.php` config file.

For the [Currencylayer](https://currencylayer.com/) api free version we can only convert from USD. 

On the navigation bar we can choose between the soap and json requests. 

We can register an account and a logged in user will have their history saved and displayed.

#### To get started ####
- run `composer install`
- create `.env` file (can use copy of the `.env.example` file and rename to `.env`) 
- Generate your application encryption key using `php artisan key:generate`.
<!---  run `npm install` and `npm run dev` in development mode -->
- Add the [Currencylayer](https://currencylayer.com/) api key in `.env` [`CURRENCY_LAYER_KEY`]
- Add the database credentials in the `.env` file
- Depending on the local setup start the server and database server
- Create the database
- Run the migrations `php artisan migrate`
- Enable the soap extension in `php.ini`
