<?php

use App\Services\Greeting;
use App\Services\ConverterContract;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\ConversionController;
use App\Http\Controllers\HistoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [ConversionController::class, 'index'])->name('conversion');

Route::get('/service', function () {
    return request()->has('json') ? redirect('/?json=1') : redirect('/');
});

Route::get('/currencies/{param?}', CurrencyController::class)->name('currencies');

Route::post('/service/convert', [ConversionController::class, 'convert'])->name('service.convert');
Route::resource('/history', HistoryController::class)->only(['index', 'store']); //->middleware('auth');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';