<x-app-layout>
    <x-slot name="header">
        {{-- <div class="max-w-5xl mx-auto py-4 px-4 sm:px-6 lg:px-8">
            <h1>The Test Site</h1>
        </div> --}}
    </x-slot>

    <div class="py-12">
        <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
            <div class="">
                
                <vue-app :user="{{ json_encode(Auth::check()) }}"></vue-app>
                    
            </div>
        </div>
    </div>
</x-app-layout>