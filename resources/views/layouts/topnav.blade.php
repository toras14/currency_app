<nav class="bg-white shadow">

    <div class="flex justify-between items-center max-w-5xl mx-auto sm:px-6 lg:px-8">
            
        <div class="flex justify-start lg:w-0 lg:flex-1">
            <div class="flex-shrink-0 flex items-center">
                <a href="/">
                    <x-project-logo class="block h-10 w-auto fill-current text-gray-600" />
                </a>
            </div>
        </div>
    
        {{-- @if (Route::has('admin'))
            <a href="/admin">Admin</a>
        @endif --}}
        
        @if (Route::has('login'))
            <div class="px-6 py-4">
                <div class="hidden sm:inline-block">
                @if (request()->has('json'))
                <a href="{{ url('/service') }}" class="text-sm text-gray-400 pr-3">
                    Soap
                </a>
                @else
                <a href="{{ url('/service?json=1') }}" class="text-sm text-gray-400 pr-3">
                    Json
                </a>
                @endif
            </div>
                @auth
                    <a href="{{ url('/') }}" class="text-sm text-gray-700 pr-3">
                        {{ Auth::user()->email }}
                    </a>
                    <!-- Authentication -->
                    
                    <form method="POST" action="{{ route('logout') }}" class="inline-block">
                        @csrf
                        <a href="route('logout')" class="text-sm text-gray-700 underline"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Logout') }}
                    </a>
                    </form>
                @else
                    <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                    @endif
                @endauth
            </div>
        @endif
    </div>
</nav>