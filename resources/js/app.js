require('./bootstrap');

require('alpinejs');

// window.Vue = require('vue');
import moment from "moment";

import Vue from 'vue'

import VueApp from './App.vue';

Vue.prototype.now = function () {
    const today = new Date();
    return today.toISOString().split('T')[0];
};

// Vue.prototype.nowValue

const app = new Vue({
    el: '#app',
    components: { VueApp }
});